/// <reference path="../../../typings/globals/jquery/index.d.ts" />

import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../services/global/global.service'
import { NavService } from '../services/nav/nav.service'

declare var require: any;
declare var $: any;

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styles: [`
    /deep/ .mat-ink-bar {
        background: none;
    }
  `]
})
export class NavComponent implements OnInit {
  nav: any[]
  cat: any[]
  branding: any
  social: any[]

  constructor(private globalService: GlobalService, private navService: NavService) { }
  ngOnInit() {
    this.branding = this.globalService
    this.nav = this.navService.getNavs()
    this.cat = this.navService.getCats()
    this.social = this.navService.getSocials()

    // HEADER SCROLLING
    var didScroll;
    var lastScrollTop = 0;
    var delta = 160;
    var navbarHeight = $('header').outerHeight();

    $(window).scroll(function(event) {
      didScroll = true;
    });

    setInterval(function() {
      if (didScroll) {
        hasScrolled();
        didScroll = false;
      }
    }, 500);

    function hasScrolled() {
      var st = $(this).scrollTop();

      // Make sure they scroll more than delta
      if (Math.abs(lastScrollTop - st) <= delta)
        return;

      if (st > lastScrollTop && st > navbarHeight) {
        // Scroll Down
        $('html').removeClass('nav-down').addClass('nav-up');
      } else {
        // Scroll Up
        $('html').addClass('nav-down').removeClass('nav-up');
      }

      lastScrollTop = st;
    }


    // $('.nav-link').click(function(e) {
    //   $('.navbar-collapse').collapse('toggle');
    // });
  }
}

// Fixed Heder js/jQuery
