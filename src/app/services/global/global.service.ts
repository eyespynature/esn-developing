import { Injectable } from '@angular/core'

@Injectable()
export class GlobalService {
    logo = '/assets/branding/logo-white2x.png';
    logoNoCircle = '/assets/branding/logo-white-no-circle2x.png';
    logoStampBlack = '/assets/branding/logo-black-stamp-only-2x.png';
    logoStampWhite = '/assets/branding/logo-white-stamp-only-2x.png';
}
