import { Injectable } from '@angular/core';

@Injectable()
export class PrintInfoService {

  getPrintDetail() {
    return DETAILS
  }

  getPrintDetails(id: any) {
    return DETAILS.find(detail => detail.id === id)
  }
}


const DETAILS = [
  {
    id: 'framed',
    name: 'Framed Prints',
    images: [
      {
        id: 'landscape',
        class: 'wall-print landscape',
        ref: 'assets/inprnt/landscape-bkFrame.png'
      },

      {
        id: 'landscape',
        class: 'wall-print lanscape',
        ref: 'assets/inprnt/landscape-whFrame.png'
      },
      {
        id: 'portrait',
        class: 'wall-print portrait',
        ref: 'assets/inprnt/portrait-naFrame.png'
      }
    ],
    formatClass: 'row mockups framed-prints',
    info: 'Eye Spy Nature gallery-quality giclée art print on 100% cotton rag archival paper, printed with archival inks. Each art print is listed by sheet size and features a minimum one-inch border. Frames are available in black, natural or white.',
    conditions: '*Please note that framed prints are only available within the USA.',
    cta: 'https://www.inprnt.com/frames/eyespynature/'
  },
  {
    id: 'cases',
    name: 'Smartphone cases',
    images: [
      {
        id: 'iphone-new',
        class: 'phone',
        ref: 'assets/inprnt/iphone.png'
      },
      {
        id: 'galaxy',
        class: 'phone',
        ref: 'assets/inprnt/galaxy.png'
      },
      {
        id: 'iphone-old',
        class: 'phone',
        ref: 'assets/inprnt/iphone-old.png'
      },
      {
        id: '',
        class: 'phone',
        ref: 'assets/inprnt/phone-random-1.png'
      },
      {
        id: '',
        class: 'phone',
        ref: 'assets/inprnt/phone-random-2.png'
      },
      {
        id: '',
        class: 'phone',
        ref: 'assets/inprnt/phone-random-3.png'
      },
      {
        id: '',
        class: 'phone',
        ref: 'assets/inprnt/phone-random-4.png'
      },
      {
        id: '',
        class: 'phone',
        ref: 'assets/inprnt/phone-random-5.png'
      }
    ],
    formatClass: 'row mockups smartphones',
    info: 'Protect your phone with this slim Eye Spy Nature profile case. The impact resistant hard shell covers the back and sides, while the lay-flat feature protects the front of your device by extending the bezel above the screen.',
    cta: 'https://www.inprnt.com/cases/eyespynature/'
  },
  {
    id: 'art',
    name: 'Art Prints',
    images: [
      {
        id: 'landscape-nb',
        class: 'art-print landscape',
        ref: 'assets/inprnt/landscape-noborder.png'
      },
      {
        id: 'portrait-nb',
        class: 'art-print portrait',
        ref: 'assets/inprnt/portrait-noborder.png'
      }
    ],
    formatClass: 'row mockups art-prints',
    info: 'Eye Spy Nature gallery-quality giclée art print on 100% cotton rag archival paper, printed with archival inks. Each art print is listed by sheet size and features a minimum one-inch border.',
    cta: 'https://www.inprnt.com/gallery/eyespynature/'
  },
  {
    id: 'cards',
    name: 'Art Cards',
    images: [
      {
        id: 'landscape',
        class: 'folded-card',
        ref: 'assets/inprnt/card.png'
      },
      {
        id: 'landscape',
        class: 'folded-card',
        ref: 'assets/inprnt/card_upright.png'
      }
    ],
    formatClass: 'row mockups art-cards',
    info: 'Eye Spy Nature photography on soft textured natural white stationery cards, archival-rated with beautiful color reproduction. Each folded card includes a natural white envelope perfect for mailing. Card dimensions are approximately 5.5" x 7.5".',
    cta: 'https://www.inprnt.com/cards/eyespynature/'
  }
]
