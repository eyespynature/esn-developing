import { TestBed, inject } from '@angular/core/testing';

import { PrintInfoService } from './print-info.service';

describe('PrintInfoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PrintInfoService]
    });
  });

  it('should be created', inject([PrintInfoService], (service: PrintInfoService) => {
    expect(service).toBeTruthy();
  }));
});
