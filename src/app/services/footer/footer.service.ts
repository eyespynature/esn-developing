import { Injectable } from '@angular/core';

@Injectable()
export class FooterService {

  getFooter() {
    return KEYWORDS
  }

  getLegals() {
    return LEGALS
  }

  constructor() { }

}

const LEGALS = [
  {
    copyright: '© 2017 Eye Spy Nature. All rights reserved. Made In Melbourne.'
    }
]


const KEYWORDS = [
  {
    id: 'Keywords',
    keywords: [
      {
        label: 'Flower photography'
      },
      {
        label: 'Leaf photography'
      },
      {
        label: 'Seasonal photography'
      },
      {
        label: 'Botanical photography'
      },
      {
        label: 'Nature photography'
      },
      {
        label: 'Digital photography'
      },
      {
        label: 'Macro photography'
      },
      {
        label: 'Autumnal photography'
      },
      {
        label: 'Winter photography'
      },
      {
        label: 'Spring photography'
      },
      {
        label: 'Summer photography'
      },
      {
        label: 'Fall photography'
      },
      {
        label: 'Forest photography'
      },
      {
        label: 'Garden photography'
      },
      {
        label: 'Fungai photography'
      },
      {
        label: 'Wood photography'
      },
      {
        label: 'Tree photography'
      },
      {
        label: 'Landscape photography'
      },
      {
        label: 'Growing'
      },
      {
        label: 'Wild photography'
      },
      {
        label: 'Fresh photography'
      },
      {
        label: 'Environmental photography'
      },
      {
        label: 'Stock photography'
      },
      {
        label: 'Art prints'
      },
      {
        label: 'Framed prints'
      },
      {
        label: 'Smartphone cases'
      },
      {
        label: 'Cloud photography'
      },
      {
        label: 'Bloom'
      },
      {
        label: 'Valantine'
      },
      {
        label: 'Romantic photography'
      },
      {
        label: 'Romance'
      },
      {
        label: 'Melbourne photography'
      },
      {
        label: 'Australian photography'
      },
      {
        label: 'Bespoke photography'
      },
      {
        label: 'Insect photography'
      },
      {
        label: 'National Geographic photography'
      },
      {
        label: 'INPRNT photography'
      },
      {
        label: 'Creative Market photography'
      },
      {
        label: 'Eye Spy Nature'
      },
      {
        label: 'Pinterest photography'
      },
      {
        label: 'Instagram photography'
      }
    ]
  }
]
