import { Injectable } from '@angular/core';

@Injectable()
export class RecentService {

    getImages() {
        return IMAGES
    }
    getImage(id:any) {
        return IMAGES.find(image => image.id === id)
    }
}


const IMAGES = [
    {
        id: 'ESN-LVE-0000001',
        ref: 'leaves/ESN-LVE-0000001.jpg',
        orientation: 'portrait',
        title: 'You\'re so vein',
        camera: 'Canon EOS 7D Mark II',
        focal: '',
        shutter: '',
        aperture: '',
        ISO: '',
        stockShop: 'https://creativemarket.com/EyeSpyNature/1615582-Your-So-Vein',
        printShop: 'https://www.inprnt.com/gallery/eyespynature/esn-lve-0000001/'
    },
    {
        id: 'ESN-ABS-0000002',
        ref: 'abstract/ESN-ABS-0000002-v2.jpg',
        orientation: 'landscape',
        title: 'ESN-FNG-0000008',
        camera: 'Canon EOS 7D Mark II',
        focal: '',
        shutter: '',
        aperture: '',
        ISO: '',
        stockShop: 'https://creativemarket.com/EyeSpyNature/1616091-Delicate',
        printShop: 'https://www.inprnt.com/gallery/eyespynature/esn-abs-0000002/'
    },
    {
        id: 'ESN-LVE-0000002',
        ref: 'leaves/ESN-LVE-0000002.jpg',
        orientation: 'landscape',
        title: 'Electric Blue',
        camera: 'Canon EOS 7D Mark II',
        focal: '400 mm',
        shutter: '1/400 sec',
        aperture: 'f/5.6',
        ISO: 250,
        stockShop: 'https://creativemarket.com/EyeSpyNature/1611596-Electric-Blue',
        printShop: 'https://www.inprnt.com/gallery/eyespynature/esn-lve-0000002/'
    },
    {
        id: 'ESN-FLW-0000002',
        ref: 'flowers/ESN-FLW-0000002.jpg',
        orientation: 'landscape',
        title: 'ESN-FLW-0000002',
        camera: 'Canon EOS 7D Mark II',
        focal: '',
        shutter: '',
        aperture: '',
        ISO: '',
        stockShop: 'https://creativemarket.com/EyeSpyNature/1613768-Pure-petals',
        printShop: 'https://www.inprnt.com/gallery/eyespynature/esn-flw-0000002/'
    },
    {
        id: 'ESN-FLW-0000003',
        ref: 'flowers/ESN-FLW-0000003.jpg',
        orientation: 'landscape',
        title: 'ESN-FLW-0000003',
        camera: 'Canon EOS 7D Mark II',
        focal: '',
        shutter: '',
        aperture: '',
        ISO: '',
        stockShop: 'https://creativemarket.com/EyeSpyNature/1610164-Rose',
        printShop: 'https://www.inprnt.com/gallery/eyespynature/esn-flw-0000003/'
    },
    {
        id: 'ESN-FLW-0000004',
        ref: 'flowers/ESN-FLW-0000004.jpg',
        orientation: 'landscape',
        title: 'ESN-FLW-0000004',
        camera: 'Canon EOS 7D Mark II',
        focal: '',
        shutter: '',
        aperture: '',
        ISO: '',
        stockShop: 'https://creativemarket.com/EyeSpyNature/1609602-Violet-flower',
        printShop: 'https://www.inprnt.com/gallery/eyespynature/esn-flw-0000004/'
    },
    {
        id: 'ESN-FLW-0000005',
        ref: 'flowers/ESN-FLW-0000005.jpg',
        orientation: 'landscape',
        title: 'ESN-FLW-0000005',
        camera: 'Canon EOS 7D Mark II',
        focal: '',
        shutter: '',
        aperture: '',
        ISO: '',
        stockShop: 'https://creativemarket.com/EyeSpyNature/1615166-Tubular-Belles',
        printShop: 'https://www.inprnt.com/gallery/eyespynature/esn-flw-0000005/'
    },
    {
        id: 'ESN-FLW-0000006',
        ref: 'flowers/ESN-FLW-0000006.jpg',
        orientation: 'landscape',
        title: 'ESN-FLW-0000006',
        camera: 'Canon EOS 7D Mark II',
        focal: '',
        shutter: '',
        aperture: '',
        ISO: '',
        stockShop: 'https://creativemarket.com/EyeSpyNature/1609443-Alien-Buddy',
        printShop: 'https://www.inprnt.com/gallery/eyespynature/esn-flw-0000006/'
    },
    {
        id: 'ESN-FLW-0000010',
        ref: 'flowers/ESN-FLW-0000010.jpg',
        orientation: 'landscape',
        title: 'ESN-FLW-0000010',
        camera: 'Canon EOS 7D Mark II',
        focal: '',
        shutter: '',
        aperture: '',
        ISO: '',
        stockShop: 'https://creativemarket.com/EyeSpyNature/1615618-Summertime-extended',
        printShop: 'https://www.inprnt.com/gallery/eyespynature/esn-flw-0000010/'
    },
    {
        id: 'ESN-FLW-0000011',
        ref: 'flowers/ESN-FLW-0000011.jpg',
        orientation: 'landscape',
        title: 'ESN-FLW-0000011',
        camera: 'Canon EOS 7D Mark II',
        focal: '',
        shutter: '',
        aperture: '',
        ISO: '',
        stockShop: 'https://creativemarket.com/EyeSpyNature/1615606-Luna-Lander',
        printShop: 'https://www.inprnt.com/gallery/eyespynature/ens-flw-0000011/'
    },
    {
        id: 'ESN-FLW-0000014',
        ref: 'flowers/ESN-FLW-0000014.jpg',
        orientation: 'landscape',
        title: 'ESN-FLW-0000014',
        camera: 'Canon EOS 7D Mark II',
        focal: '',
        shutter: '',
        aperture: '',
        ISO: '',
        stockShop: 'https://creativemarket.com/EyeSpyNature/1608512-Antenna-Flower',
        printShop: 'https://www.inprnt.com/gallery/eyespynature/esn-flw-0000014/'
    },
    {
        id: 'ESN-FLW-0000023',
        ref: 'flowers/ESN-FLW-0000023.jpg',
        title: 'ESN-FLW-0000023',
        camera: 'Canon EOS 7D Mark II',
        focal: '',
        shutter: '',
        aperture: '',
        ISO: '',
        stockShop: 'https://creativemarket.com/EyeSpyNature/1615633-Above-Beyond',
        printShop: 'https://www.inprnt.com/gallery/eyespynature/esn-flw-0000023/'
    },
    {
        id: 'ESN-FLW-0000024',
        ref: 'flowers/ESN-FLW-0000024.jpg',
        orientation: 'landscape',
        title: 'ESN-FLW-0000024',
        camera: 'Canon EOS 7D Mark II',
        focal: '',
        shutter: '',
        aperture: '',
        ISO: '',
        stockShop: 'https://creativemarket.com/EyeSpyNature/1608465-Got-the-blues',
        printShop: 'https://www.inprnt.com/gallery/eyespynature/esn-flw-0000024/'
    },
    {
        id: 'ESN-LVE-0000007',
        ref: 'leaves/ESN-LVE-0000007.jpg',
        orientation: 'landscape',
        title: 'ESN-LVE-0000007',
        camera: 'Canon EOS 7D Mark II',
        focal: '',
        shutter: '',
        aperture: '',
        ISO: '',
        stockShop: 'https://creativemarket.com/EyeSpyNature/1624584-Can-you-feel-it',
        printShop: 'https://www.inprnt.com/gallery/eyespynature/esn-lve-0000007/'
    },
    {
        id: 'ESN-FNG-0000004',
        ref: 'fungai/ESN-FNG-0000004.jpg',
        orientation: 'landscape',
        title: 'ESN-LVE-0000004',
        camera: 'Canon EOS 7D Mark II',
        focal: '',
        shutter: '',
        aperture: '',
        ISO: '',
        stockShop: 'https://creativemarket.com/EyeSpyNature/1615659-Under-Control',
        printShop: 'https://www.inprnt.com/gallery/eyespynature/esn-fng-0000004/'
    },
    {
        id: 'ESN-FNG-0000006',
        ref: 'fungai/ESN-FNG-0000006.jpg',
        orientation: 'landscape',
        title: 'ESN-FNG-0000006',
        camera: 'Canon EOS 7D Mark II',
        focal: '',
        shutter: '',
        aperture: '',
        ISO: '',
        stockShop: 'https://creativemarket.com/EyeSpyNature/1609574-Little-Toadstool',
        printShop: 'https://www.inprnt.com/gallery/eyespynature/esn-fng-0000006/'
    },
    {
        id: 'ESN-FLW-0000013',
        ref: 'flowers/ESN-FLW-0000013.jpg',
        orientation: 'portrait',
        title: 'ESN-FLW-0000013',
        camera: 'Canon EOS 7D Mark II',
        focal: '',
        shutter: '',
        aperture: '',
        ISO: '',
        stockShop: 'https://creativemarket.com/EyeSpyNature/1611581-Distant-Purple-Flower',
        printShop: 'https://www.inprnt.com/gallery/eyespynature/esn-flw-0000013/'
    },
]
