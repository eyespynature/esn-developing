import { TestBed, inject } from '@angular/core/testing';

import { GalleryCategoriesService } from './gallery-categories.service';

describe('GalleryCategoriesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GalleryCategoriesService]
    });
  });

  it('should be created', inject([GalleryCategoriesService], (service: GalleryCategoriesService) => {
    expect(service).toBeTruthy();
  }));
});
