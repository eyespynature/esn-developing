import { Injectable } from '@angular/core'

@Injectable()
export class NavService {
    // Menu
    getNavs() {
        return NAVIGATION
    }
    getNav(id:any) {
        return NAVIGATION.find(nav => nav.id === id)
    }
    // GALLERY CATEGORIES
    getCats() {
        return GALLERY
    }
    getCat(id:any) {
        return GALLERY.find(cat => cat.id === id)
    }
     // Social Media
    getSocials() {
        return SOCIALS
    }
    getSocial(id:any) {
        return SOCIALS.find(social => social.id === id)
    }

    getBuys() {
      return BUY
    }
}



const NAVIGATION = [
    {
        id: 'about',
        title: 'about',
        link: '/about'
    },
    {
        id: 'print',
        title: 'print info',
        link: '/print'
    },
    {
        id: 'contact',
        title: 'contact',
        link: '/contact'
    }
]

const GALLERY = [
     {
        id: 'flowers',
        title: 'flowers',
        link: 'flowers'
    },
    {
        id: 'leaves',
        title: 'leaves',
        link: 'leaves'
    },
    {
        id: 'insects',
        title: 'insects',
        link: 'insects'
    },
    {
        id: 'fungai',
        title: 'fungai',
        link: 'fungai'
    }
]
const BUY = [
  {
      id: 'creative-market',
      title: 'creative_market',
      url: 'https://creativemarket.com/EyeSpyNature',
      class: 'creative_market'
  },
  {
      id: 'inprnt',
      title: 'inprnt',
      url: 'https://www.inprnt.com/gallery/eyespynature/',
      class: 'inprnt'
  },
  {
      id: 'shutter',
      title: 'shutterstock',
      url: 'https://www.shutterstock.com/g/eyespynature_au',
      class: 'shutter'
  },
  {
      id: 'zazzle',
      title: 'zazzle',
      url: 'https://www.shutterstock.com/g/eyespynature_au',
      class: 'zazzle'
  }
]

const SOCIALS = [
    {
        id: 'insta',
        title: 'instagram',
        url: 'http://www.instagram.com/eyespynature_au',
        class: 'instagram'
    },
    {
        id: 'pint',
        title: 'pinterest',
        url: 'https://au.pinterest.com/eyespynature/',
        class: 'pinterest'
    },
    {
        id: 'natgeo',
        title: 'natgeo',
        url: 'http://yourshot.nationalgeographic.com/profile/1541554/',
        class: 'natgeo'
    }
]
