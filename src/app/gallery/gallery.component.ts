import { Component, OnInit } from '@angular/core';
import { GalleryCategoriesService } from '../services/gallery/gallery-categories.service';

@Component({
  selector: 'gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.css']
})

export class GalleryComponent implements OnInit {
    // pageTitle: string

    constructor () {
    }

    ngOnInit() {
        // this.pageTitle = 'Gallery'
	      var root = document.documentElement;
        root.className += ' landing-page';

    }
}
