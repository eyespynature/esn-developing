import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router';
import { GalleryCategoriesService } from '../../services/gallery/gallery-categories.service';
import { Router }  from '@angular/router';


@Component ({
    selector: 'gallery-list',
    templateUrl: './gallery-list.component.html',
    styles: [`
        #ngBoxWrapper[_ngcontent-c5] {
            left: 0;
            right: 0;
        }
    `]
})

export class GalleryListComponent implements OnInit {
    categories:any[]
    images:any[]
    pageTitle: string

    constructor(
        private galleryCategoriesService: GalleryCategoriesService) {

    }


    ngOnInit() {
        this.categories = this.galleryCategoriesService.getCategories()
        var root = document.documentElement;
        root.className += ' child-page';
        this.pageTitle = 'Gallery';
    }
}
