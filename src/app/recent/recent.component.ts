import { Component, OnInit } from '@angular/core';
import { RecentService } from '../services/recent/recent.service';

@Component({
    selector: '<recent-work></recent-work>',
  templateUrl: './recent.component.html',
  styleUrls: ['./recent.component.css']
})
export class RecentComponent {

    pageTitle: string;

    constructor(private recentService: RecentService) {
        this.pageTitle = 'Recent Work';

           
    }
}



