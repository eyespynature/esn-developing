import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecentCollectionComponent } from './recent-collection.component';

describe('RecentCollectionComponent', () => {
  let component: RecentCollectionComponent;
  let fixture: ComponentFixture<RecentCollectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecentCollectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecentCollectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
