/// <reference path="../../../../../typings/modules/masonry-layout/index.d.ts" />
import { Component, OnInit } from '@angular/core';
import { RecentService} from '../../../services/recent/recent.service';
import { GalleryCategoriesService } from '../../../services/gallery/gallery-categories.service';
declare var Masonry:any;

@Component({
  selector: 'recent-image',
  templateUrl: './recent-collection.component.html',
  styleUrls: ['./recent-collection.component.css']
})
export class RecentCollectionComponent implements OnInit {

    openModalWindow:boolean=false;
    imagePointer:number;
    images:any[]
    gallery:any[]
    showAllImages: boolean;

    constructor(
      private galleryCategoriesService: GalleryCategoriesService,
      private recentService: RecentService) {
    }

    ngOnInit() {
        this.images = this.recentService.getImages()
        this.gallery = this.galleryCategoriesService.getCategories()
    }

    // toggleAllImages(){
    //     if(this.showAllImages == true) {
    //         this.showAllImages = false;
    //     } else {
    //         this.showAllImages = true;
    //     }
    // }
 }
