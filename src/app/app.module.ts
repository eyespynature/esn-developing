import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { MaterialModule, MdButtonModule, MdCardModule, MdMenuModule, MdToolbarModule, MdIconModule, MdSidenavModule, MdTabsModule } from '@angular/material';
import { NgModule } from '@angular/core';
import { ResponsiveModule, ResponsiveConfig  } from 'ng2-responsive';
import { NgBoxModule } from 'ngbox/ngbox.module';
import { NgBoxService } from 'ngbox/ngbox.service';
// import { GalleryModule,GalleryConfig  } from 'ng-gallery';
import { Ng2SearchPipeModule } from 'ng2-search-filter';




import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { AboutComponent } from './about';
import { ContactComponent } from './contact';
import { DigitalComponent } from './digital';
import { FooterService } from './services/footer/footer.service';
import { GalleryComponent } from './gallery';
import { GalleryListComponent } from './gallery/gallery-list';
import { GalleryCategoriesService } from './services/gallery/gallery-categories.service';
import { GlobalService } from './services/global/global.service';
import { HeroService } from './services/hero/hero.service';
import { HeroComponent } from './hero';
import { HomeComponent } from './home';
import { NavComponent } from './nav';
import { NavService } from './services/nav/nav.service';
import { PrintComponent } from './print';
import { PrintInfoService } from './services/print-info/print-info.service'
import { RecentCollectionComponent } from './recent/collection/recent-collection';
import { RecentComponent } from './recent';
import { RecentService } from './services/recent/recent.service';
import { PageTitleComponent } from './page-title';
import { FooterComponent } from './footer';

// ng-responsie config
let config = {
    breakPoints: {
        xs: {max: 0}, // not used
        sm: {min: 100, max: 767}, //phone-up
        md: {min: 769, max: 3000}, //desktop-down
        lg: {min: 0, max: 0}, // not used
        xl: {min: 3000}
    },
    debounceTime: 0 // allow to debounce checking timer
  };

  export function ResponsiveDefinition(){
          return new ResponsiveConfig(config);
  };

  // export const galleryConfig: GalleryConfig = {
  //
  // }


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    GalleryComponent,
    RecentComponent,
    AboutComponent,
    ContactComponent,
    PrintComponent,
    DigitalComponent,
    NavComponent,
    HeroComponent,
    GalleryListComponent,
    RecentCollectionComponent,
    PageTitleComponent,
    FooterComponent
      ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    MaterialModule,
    BrowserAnimationsModule,
    MaterialModule,
    MdButtonModule,
    MdCardModule,
    MdMenuModule,
    MdToolbarModule,
    MdIconModule,
    MdSidenavModule,
    MdTabsModule,
    AppRoutingModule,
    ResponsiveModule,
    NgBoxModule,
    Ng2SearchPipeModule,
    // GalleryModule.forRoot(galleryConfig)
    ],
  providers: [
    NavService,
    GlobalService,
    HeroService,
    GalleryCategoriesService,
    RecentService,
    NgBoxService,
    PageTitleComponent,
    PrintInfoService,
    FooterService,
    {
      provide: ResponsiveConfig,
      useFactory: ResponsiveDefinition
    }],
  bootstrap: [
    AppComponent
    ]
})
export class AppModule { }
