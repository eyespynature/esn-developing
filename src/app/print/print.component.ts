import { Component, OnInit } from '@angular/core';
import { PrintInfoService } from '../services/print-info/print-info.service';

@Component({
  selector: '<print></print>',
  templateUrl: './print.component.html',
  styleUrls: ['./print.component.css']
})
export class PrintComponent implements OnInit {
  pageTitle: string
  details:any[]

  constructor (private printInfoService: PrintInfoService) {

   }

  ngOnInit() {
    this.details = this.printInfoService.getPrintDetail()
    this.pageTitle = 'Print Information'
    var root = document.documentElement;
    root.className += ' child-page';

  }
}
