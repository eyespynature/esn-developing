/// <reference path="../../../typings/globals/jquery/index.d.ts" />

import { Component, OnInit, trigger, state, style, transition, animate, keyframes } from '@angular/core';
import { HeroService } from '../services/hero/hero.service';
import { GlobalService } from '../services/global/global.service';

declare var require: any;
declare var $: any;

@Component({
  selector: 'hero',
  templateUrl: './hero.component.html',
  styleUrls: ['./hero.component.css']
})
export class HeroComponent implements OnInit {

  heros: any[]
  branding: any

  constructor(private heroService: HeroService, private globalService: GlobalService) { }

  ngOnInit() {
    this.heros = this.heroService.getHeros()
    this.branding = this.globalService

    // Anchor scroll
    var smoothScroll = require('smoothscroll');
    var exampleBtn = document.querySelector('.hero-cta');
    var exampleDestination = document.querySelector('.recent-work');
    var handleClick = function(event) {
      event.preventDefault();
      smoothScroll(exampleDestination);
    };
    exampleBtn.addEventListener('click', handleClick);
  }
}
