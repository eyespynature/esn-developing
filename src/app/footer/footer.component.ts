import { Component, OnInit } from '@angular/core';
import { FooterService } from '../services/footer/footer.service';
import { NavService } from '../services/nav/nav.service';

@Component({
  selector: 'footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  footer: any[]
  social: any[]
  buy: any[]
  legal: any[]

  constructor(private footerService: FooterService, private navService: NavService) { }

  ngOnInit() {
    this.footer = this.footerService.getFooter()
    this.social = this.navService.getSocials()
    this.buy = this.navService.getBuys()
    this.legal = this.footerService.getLegals()
  }
}
