import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router'
import { HomeComponent } from './home';
import { AboutComponent } from './about';
import { GalleryComponent } from './gallery';
import { GalleryListComponent } from './gallery/gallery-list';
// import { GalleryCategoryComponent } from './gallery/gallery-category';
import { ContactComponent } from './contact';
import { PrintComponent } from './print';

const routes: Routes = [
    {
        path: '',
        redirectTo: '/',
        pathMatch: 'full'
    },
    {
        path: '',
        component: HomeComponent
    },
    {
        path: 'about',
        component: AboutComponent
    },
    {
        path: 'contact',
        component: ContactComponent
    },
    {
        path: 'gallery',
        component: GalleryComponent
    },
    {
        path: 'print',
        component: PrintComponent
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
    providers: []
})

export class AppRoutingModule {}
