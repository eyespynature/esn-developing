import { EyespynaturePage } from './app.po';

describe('eyespynature App', () => {
  let page: EyespynaturePage;

  beforeEach(() => {
    page = new EyespynaturePage();
  });

  it('should display welcome message', done => {
    page.navigateTo();
    page.getParagraphText()
      .then(msg => expect(msg).toEqual('Welcome to app!!'))
      .then(done, done.fail);
  });
});
